"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //Mảng chứa dữ liệu môn học
    const gNOT_SELECT_SUBJECT = "NOT_SELECT_SUBJECT";
    //Mảng chứa dữ liệu tên
    const gNOT_SELECT_STUDENT = "NOT_SELECT_STUDENT";
    var gGradesData = []; //Khai báo biến mảng (biến toàn cục) để chứa thông tin
    const gGRADE = ["stt", "studentId", "subjectId", "grade", "examDate", "action"];
    //Khai báo các cột của datatable
    const gCOLUMN_STT = 0;
    const gCOLUMN_STUDENT = 1;
    const gCOLUMN_SUBJECT_COL = 2;
    const gCOLUMN_GRADE_COL = 3;
    const gCOLUMN_EXAM_DATE_COL = 4;
    const gCOLUMN_ACTION = 5;
    var gStt = 0;
    var gStudentName;
    var gSubject;
    //Biến toàn cục để lưu trữ id đang được update hoặc delete
    var gStudentId = 0;
    //Định nghĩa table - chưa có data
    var gStudentTable = $('#table-users').DataTable({
        "columns": [
            { "data": gGRADE[gCOLUMN_STT] },
            { "data": gGRADE[gCOLUMN_STUDENT] },
            { "data": gGRADE[gCOLUMN_SUBJECT_COL] },
            { "data": gGRADE[gCOLUMN_GRADE_COL] },
            { "data": gGRADE[gCOLUMN_EXAM_DATE_COL] },
            { "data": gGRADE[gCOLUMN_ACTION] },
        ],
        //Các thuộc tính của USER sẽ match với từng COLUMN
        "columnDefs": [
            //Định nghĩa các cột cần hiện ra
            {
                /*
                targets: gCOLUMN_STT,
                render: function () {
                    debugger;
                    return gStt++
                }
                */
                "targets": gCOLUMN_STT, //cột STT
                "className": "text-center",
                render: renderStt

            },
            {
                "targets": gCOLUMN_STUDENT, //cột Sinh Viên
                "className": "text-center text-primary",
                render: renderStudentIdToStudentName
            },
            {
                "targets": gCOLUMN_SUBJECT_COL, //cột Môn Học
                "className": "text-center text-primary",
                render: renderSubjectIdToSubjectName
            },
            {
                "targets": gCOLUMN_GRADE_COL, //cột Điểm
                "className": "text-center text-primary",
            },
            {
                "targets": gCOLUMN_EXAM_DATE_COL, //cột Ngày Thi
                "className": "text-center",
            },
            //Ghi đè nội dung của cột action, chuyển thành button "Sửa" "Xoá"
            {
                "targets": gCOLUMN_ACTION,
                "className": "text-center",
                "defaultContent": `
                <div class="row">
                <div class = "col-md-5">
                    <button class="btn btn-primary btn-edit"> <i class="far fa-edit"></i></button>
                </div>
                <div class = "col-md-5">
                    <button class="btn btn-danger btn-delete"> <i class="far fa-trash-alt"></i></button>
                </div>
            </div>
            `
            }
        ]
    });

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //Hàm chạy khi trang được load
    getStudentNameAjaxList();
    getStudentNameAjaxListOnCreateModal();
    getStudentNameAjaxListOnEditModal();
    getSubjectNameAjaxList();
    getSubjectNameAjaxListOnCreateModal();
    getSubjectNameAjaxListOnEditModal();
    onPageLoading();
    //Gán sự kiện cho nút 'Lọc dữ liệu'
    $("#btn-filter-data").on("click", function () {
        onBtnFilterClick();
    });
    //Gắn event handler (sự kiện) cho nút 'Thêm'
    //Gán sự kiện Create - Thêm mới Student  
    $("#btn-add-student").on("click", function () {
        onBtnAddStudentClick();
    });
    //C: Gán sự kiện Create - Thêm mới thông tin Student
    $("#btn-create-student").on("click", function () {
        onBtnCreateStudentClick();
    });
    //U: Gán sự kiện Edit - Sửa thông tin Student
    $("#table-users").on("click", ".btn-edit", function () {
        onBtnEditClick(this);
    });
    //Gán sự kiện khi ấn nút 'Cập nhật' trên  Modeal Edit
    $("#btn-edit-student").on("click", function () {
        onBtnConfirmEditClick();
    });
    //D: Gán sự kiện Delete - Xoá thông tin Student
    $("#table-users").on("click", ".btn-delete", function () {
        onBtnDeleteClick(this);
    });
    //Gán sự kiện khi ấn nút 'Xoá' trên Modal Delete
    $("#btn-confirm-delete-student").on("click", function () {
        onBtnConfirmDeleteClick();
    });

    //Gắn event handler (sự kiện) cho nút 'Sửa'
    //Gắn event handler (sự kiện) cho nút 'Xoá'

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm gọi API để load data vào trang 
    function onPageLoading() {
        //Lấy data từ sever
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                gGradesData = responseObject;
                console.log(gGradesData);
                loadDataToTable(gGradesData);
            },
            error: function (error) {
                console.assert(error.responseText);
            }
        });
    }
    //Hàm render STT
    function renderStt() {
        gStt++;
        return gStt;
    }
    //Hàm gọi API để load data students vào ô select Sinh Viên
    function getStudentNameAjaxList() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/students',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handleStudentList(responseObject);
                gStudentName = responseObject;
                console.log(gStudentName);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm gọi API để load data students vào ô select Sinh Viên (trên Modal Create)
    function getStudentNameAjaxListOnCreateModal() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/students',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handleStudentListOnCreateModal(responseObject);
                gStudentName = responseObject;
                console.log(gStudentName);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm API để load data student vào ô select Sinh Viên (trên Modal Edit)
    function getStudentNameAjaxListOnEditModal() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/students',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handelStudentListDetailOnEditModal(responseObject);
                gStudentName = responseObject;
                console.log(gStudentName);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm gọi API  để load data môn học vào ô Môn học 
    function getSubjectNameAjaxList() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/subjects',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handleSubjectList(responseObject);
                gSubject = responseObject;
                console.log(gSubject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm gọi API để load data môn học vào ô Môn học (trên Modal Create)
    function getSubjectNameAjaxListOnCreateModal() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/subjects',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handleSubjectListOnCreateModal(responseObject);
                gSubject = responseObject;
                console.log(gSubject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm gọi API để load data môn học vào ô môn học (trên Modal Edit)
    function getSubjectNameAjaxListOnEditModal() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/subjects',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                handelSubjectListOnEditModal(responseObject);
                gSubject = responseObject;
                console.log(gSubject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    //Hàm xử lý khi ấn nút "Thêm"
    function onBtnAddStudentClick() {
        "use strict";
        $("#modal-user-detail").modal("show");
    }
    //Hàm xử lý sự kiện khi ấn nút "Thêm mới" (trên Modal "Thêm")
    function onBtnCreateStudentClick() {
        //Khai báo đối tượng chứa Student Data
        var vStudentObj = {
            studentId: "",
            subjectId: "",
            grade: "",
            examDate: ""
        };
        //Bước 1: Thu thập dữ liệu
        getCreateStudentData(vStudentObj);
        //Bước 2: Validate Insert
        var vIsStudentDataValidate = validateStudentData(vStudentObj);
        if (vIsStudentDataValidate) {
            //Bước 3: Insert Student Data
            postStudentApi(vStudentObj);
            //Bước 4: Xử lý Front-end
            //Gọi API lấy danh sách student
            getAllStudent();
            //Load lại vào bảng (table)
            loadDataToTable(gGradesData);
            //Tắt (ẩn) modal form Update user
            $('#modal-user-detail').modal('hide');
            //Xoá trắng dữ liệu trên modal
            resertCreateStudentForm();
        }
    }
    //Hàm gọi API get student
    function getAllStudent() {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (res) {
                //Lấy response trả về và gán cho biến toàn cục gStudentData
                gGradesData = res;
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }
    //Hàm xoá trắng form create student
    function resertCreateStudentForm() {
        $("#select-student-id").val("");
        $("#select-subject-id").val("");
        $("#input-grade").val("");
        $("#input-exam-date").val("");
    }
    //Hàm xử lý sự kiện Filter Data
    function onBtnFilterClick() {
        "use strict";
        debugger;
        var vStudentName = $("#student-Select").val();
        var vSubjetName = $("#subject-Select").val();
        var vDataFilter = gGradesData.filter(function (paramOrder) {
            return (vSubjetName === "0" || (paramOrder.subjectId != null && vSubjetName == paramOrder.subjectId))
                && (vStudentName === "0" || (paramOrder.studentId != null && vStudentName == paramOrder.studentId))
        });
        //Gọi hàm load (đổ) dữ liệu vào table
        loadDataToTable(vDataFilter);
    }
    //Hàm xử lý sự kiện khi icon Edit được ấn 
    function onBtnEditClick(paramBtnElement) {
        "use strict";
        console.log("Nút Edit được ấn!");
        var vRowClick = $(paramBtnElement).closest("tr"); //xác định tr chứa nút bấm được click
        var vTable = $("#table-users").DataTable(); //tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        console.log("ID của student tương ứng = " + vDataRow.id);
        //Lưu thông tin Id đang được Edit vào biến toàn cục
        gStudentId = getStudentIdFromButton(paramBtnElement);
        //Gọi API để truy xuất thông tin và đổ vào Modal
        getAjaxStudentDetailById(gStudentId);
        //Hiển thị Modal lên
        $("#modal-update").modal("show");
    }
    //Hàm xử lý sử kiện khi ân nút 'Cập nhật' (trên Edit Modal)
    function onBtnConfirmEditClick() {
        "use strict";
        console.log("Nút Cập nhật được ấn!");
        //Bước 0: Khai báo đối tượng chứa Student Data 
        //var vGradeId = gStudentId;
        var vGradeObjRequest = {
            studentId: "",
            subjectId: "",
            grade: "",
            examDate: ""
        };
        //Bước 1: Thu thập dữ liệu 
        getUpdateStudentData(vGradeObjRequest);
        //Bước 2: Validate dữ liệu update
        var vIsStudentDataValidate = validateStudentGradeData(vGradeObjRequest);
        if (vIsStudentDataValidate) {
            //Bước 3: Update Student Data
            //updateGrade(vGradeObjRequest);
            getAjaxUpdateStudentData(vGradeObjRequest);
            //Gọi API lấy danh sách student
            getAllStudent();
            //Bước 4: Xử lý hiển thị front-end
            alert("Sửa student data thành công!");
            $("#modal-update").modal("hide");
            loadDataToStudentTable(gGradesData);
        }
    }
    //Hàm xử lý sự kiện khi ấn icon Detele
    function onBtnDeleteClick(paramBtnElement) {
        "use strict";
        console.log("Nút Delete được ấn!");
        var vRowClick = $(paramBtnElement).closest("tr"); //xác định tr chứa nút bấm được click
        var vTable = $("#table-users").DataTable(); //tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        console.log("ID của student tương ứng = " + vDataRow.id);
        //Lưu thông tin studentId đang được edit vào biến toàn cục 
        gStudentId = getStudentIdFromButton(paramBtnElement);
        //Hiển thị Modal lên
        $("#delete-confirm-modal").modal("show");
    }
    //Hàm xử lý sự kiện khi ấn nút 'Xác nhận' (trên Delete Modal)
    function onBtnConfirmDeleteClick() {
        //Bước 1: Thu thập dữ liệu (không có)
        //Bước 2: Validate dữ liệu (không có)
        //Bước 3: Xoá Student
        getAjaxDeleteStudent(gStudentId);
        //Gọi API lấy danh sách student
        getAllStudent();
        //Bước 4: Xử lý hiển thị front-end
        alert("Xoá Student thành công!");
        $("#delete-confirm-modal").modal("hide");
        loadDataToStudentTable(gGradesData);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý đổ dữ liệu vào bảng
    function loadDataToTable(paramResponseObject) {
        "use strict";
        console.log(paramResponseObject);
        var vUserTable = $("#table-users").DataTable();
        //Xoá toàn bộ dữ liệu đang có của bảng
        vUserTable.clear();
        //Cập nhật data cho bảng
        vUserTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị cho bảng
        vUserTable.draw();
    }
    //Hàm tạo option cho Select Sinh viên
    function handleStudentList(paramStudent) {
        "use strict";
        $.each(paramStudent, function (i, item) {
            $("#student-Select").append($('<option>', {
                text: item.firstname + " " + item.lastname,
                value: item.id
            }))
        })
    }
    //Hàm tạo option cho Select Sinh viên (trên Modal Create)
    function handleStudentListOnCreateModal(paramStudent) {
        "use strict";
        $.each(paramStudent, function (i, item) {
            $("#select-student-id").append($('<option>', {
                text: item.firstname + " " + item.lastname,
                value: item.id
            }))
        })
    }
    //Hàm tạo option cho Select Sinh Viên (trên Edit Modal)
    function handelStudentListDetailOnEditModal(paramStudent) {
        "use strict";
        $.each(paramStudent, function (i, item) {
            $("#select-student-id-update").append($('<option>', {
                text: item.firstname + " " + item.lastname,
                value: item.id
            }))
        })
    }
    //Hàm tạo option cho Select Môn Học
    function handleSubjectList(paramSubject) {
        "use strict";
        $.each(paramSubject, function (i, item) {
            $("#subject-Select").append($('<option>', {
                text: item.subjectName,
                value: item.id
            }))
        })
    }
    //Hàm tạo option cho Select Môn Học (trên Create Modal)
    function handleSubjectListOnCreateModal(paramSubject) {
        $.each(paramSubject, function (i, item) {
            $("#select-subject-id").append($('<option>', {
                text: item.subjectName,
                value: item.id
            }))
        })
    }
    //Hàm tạo option Select cho Select Subject (trên Edit Modal)
    function handelSubjectListOnEditModal(paramSubject) {
        "use strict";
        $.each(paramSubject, function (i, item) {
            $("#select-subject-id-update").append($('<option>', {
                text: item.subjectName,
                value: item.id
            }))
        })

    }
    //Hàm Render Student Id sang Student Name
    function renderStudentIdToStudentName(paramStudentId) {
        "use strict";
        for (var bI = 0; bI < gStudentName.length; bI++) {
            if (paramStudentId == gStudentName[bI].id) {
                return gStudentName[bI].firstname + " " + gStudentName[bI].lastname;
            }
        }
    }
    //Hàm Render Subject Id sang Subject Name
    function renderSubjectIdToSubjectName(paramSubjectId) {
        "use strict";
        for (var bI = 0; bI < gSubject.length; bI++) {
            if (paramSubjectId == gSubject[bI].id) {
                return gSubject[bI].subjectName;
            }
        }
    }
    //Hàm thu thập dữ liệu để Create Student Data
    function getCreateStudentData(paramStudentObj) {
        "use strict";
        paramStudentObj.id = getNextId();
        paramStudentObj.studentId = $("#select-student-id").val();
        paramStudentObj.subjectId = $("#select-subject-id").val();
        paramStudentObj.grade = $("#input-grade").val().trim();
        paramStudentObj.examDate = $("#input-exam-date").val().trim();
    }
    //Hàm validate Student Data
    function validateStudentData(paramStudentObj) {
        "use strict";
        if (paramStudentObj.studentId == gNOT_SELECT_STUDENT) {
            alert("[Student] cần chọn");
            return false;
        }
        if (paramStudentObj.subjectId == gNOT_SELECT_SUBJECT) {
            alert("[Subject] cần điền vào");
            return false;
        }
        if (paramStudentObj.grade == "") {
            alert("[grade] cần điền vào");
            return false;
        }
        if (paramStudentObj.examDate == "") {
            alert("[examDate] cần điền vào");
            return false;
        }
        return true;
    }
    //Hàm lấy ra được id student tiếp theo, dùng khi thêm mới student
    function getNextId() {
        var vNextId = 0;
        //Nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
        if (gGradesData.length == 0) {
            vNextId = 1;
        }
        else {
            vNextId = gGradesData[gGradesData.length - 1].id + 1;
        }
        return vNextId;
    }
    //Hàm gọi API insert student
    function postStudentApi(paramStudentObj) {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades',
            type: 'POST',
            data: JSON.stringify(paramStudentObj),
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                alert('Thêm student thành công!');
            },
            error: function (error) {
                alert(error.responseText);
            }
        });
    }
    //Hàm dựa vào button detail (Edit or Delete) xác định được Id User
    function getStudentIdFromButton(paramBtnElement) {
        var vTableRow = $(paramBtnElement).parents("tr");
        var vUserRowData = gStudentTable.row(vTableRow).data();
        return vUserRowData.id;
    }
    //Hàm gọi API sau khi ấn nút "Xoá" trên Modal Delete
    function getAjaxDeleteStudent(paramGradeId) {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades/' + paramGradeId,
            type: 'DELETE',
            contentType: 'application/json;charset=UTF-8',
            async: false,
            success: function (res) {
                console.log(res);
            }
        });
    }
    //Hàm đổ dữ liệu vào bảng sau khi ấn nút Xác nhận (trên Modal Xoá)
    //Load user array to Datatable
    //In: student array
    //Out: student table has data
    function loadDataToStudentTable(paramStudentArray) {
        "use strict";
        gStt = 0;
        gStudentTable.clear();
        gStudentTable.rows.add(paramStudentArray);
        gStudentTable.draw();
    }
    //Hàm gọi API Detail Student theo Id
    function getAjaxStudentDetailById(paramGradeId) {
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades/' + paramGradeId,
            dataType: 'json',
            type: 'GET',
            success: function (res) {
                handleStudentDetail(res);
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);

            }
        });
    }
    //Hàm đổ data vào Modal 
    function handleStudentDetail(paramResponse) {
        "use strict";
        //$("#input-stt-update").val(paramResponse.gStt[bI].stt);
        $("#input-grade-update").val(paramResponse.grade);
        $("#input-exam-date-update").val(paramResponse.examDate);
        $("#select-student-id-update").val(paramResponse.studentId);
        $("#select-subject-id-update").val(paramResponse.subjectId);
    }
    //Hàm thu thập dữ liệu để update Student User Data (Trên Modal Edit)
    function getUpdateStudentData(paramStudentObj) {
        "use strict";
        paramStudentObj.studentId = $("#select-student-id-update").val().trim();
        paramStudentObj.subjectId = $("#select-subject-id-update").val().trim();
        paramStudentObj.grade = $("#input-grade-update").val().trim();
        paramStudentObj.examDate = $("#input-exam-date-update").val().trim();
    }
    //Hàm validate dữ liệu để update Grade theo Id (trên Modal Edit)
    function validateStudentGradeData(paramStudentObj) {
        "use strict";
        if (paramStudentObj.grade == "") {
            alert("[grade] không được để trống!");
            return false;
        }
        return true;
    }
    //Hàm gọi API student data theo Id sau khi ấn nút "Xác nhận"
    function getAjaxUpdateStudentData(paramStudentObj){
        "use strict";
        $.ajax({
            url: 'https://62454a477701ec8f724fb923.mockapi.io/api/v1/' + '/grades/' + gStudentId,
            type: 'PUT',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(paramStudentObj),
            async: false,
            success: function (res){
                console.log(res);
            },
            error: function (ajaxContext){
                alert(ajaxContext.responseText);
            }
        });

    }
    
    /*
    //Hàm update Grade của Student vào mảng (trên Modal Edit)
    function updateGrade(paramStudentObj){
        var vStudentIndex = getIndexFromStudentId(gStudentId);
        gGradesData.splice(vStudentIndex, 1, paramStudentObj);
    }
    //Hàm getIndex from studentId
    //input: paramStudentId là studentId cần tìm index
    //output: trả về chỉ số (index) trong mảng student
    function getIndexFromStudentId(paramStudentId){
        var vStudentIndex = -1;
        var vStudentFound = false;
        var vLoopIndex = 0;
        while(!vStudentFound && vLoopIndex < gGradesData.length){
            if(gGradesData[vLoopIndex].id === paramStudentId){
                vStudentIndex = vLoopIndex;
                vStudentFound = true;
            }
            else {
                vLoopIndex++;
            }
        }
        return vStudentIndex;
    }
    
    //Hàm show Student Data lên Modal
     function showDataToModal(paramResponse) {
        var vGradeObject = null;
        for (var bI = 0; bI < gGradesData.length; bI++) {
            if (paramResponse == gGradesData[bI].id) {
                vGradeObject = gGradesData[bI];
            }
        }
        if (vGradeObject != null) {
            $("#select-student-id-update").val(vGradeObject.id);
            $("#select-subject-id-update").val(vGradeObject.subjectId);
            $("#input-grade-update").val(vGradeObject.grade);
            $("#input-exam-date-update").val(vGradeObject.examDate);
        }
        else {
            $("#input-student-id-update").val("");
            $("#input-subject-id-update").val("");
            $("#input-grade-update").val("");
            $("#input-exam-date-update").val("");
        }
    }
    */
    /*
    //Hàm dựa vào Button Detail (Edit) xác định Id
    function getIdFromButton(paramBtnEdit) {
        var vRowClick = $(paramBtnEdit).closest("tr");
        var vTable = $("#table-users").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        gId = vDataRow.id
        console.log(gId);
        getAjaxStudentDetailById(gId);
        $("#modal-update").modal("show");
    }
    /*
    var vTableRow = $(paramBtnEdit).parents("tr");
    var vStudentRowData = gStudentTable.row(vTableRow).data();
    return vStudentRowData.id;
    */

});